# gnparser

gRPC client to [gnparser] project (Go version)

## Overview

``gnparser`` is a Ruby gem that allows to parse scientific names extremely
fast using remote gRPC method calls to [gnparser] written in Go.

## Install

```bash
gem install gnparser
```

## Usage

[Download][releases] the latest version of Go `gnparser`, and start it
on your local machine as a gRPC server:

```bash
gnparser -g 8778
```

Now you will be able to use Ruby `gnparser` with its default settings.

```ruby
require 'gnparser'
gnp = GNparser::Client.new

# To use different host and port:
gnp = GNparser::Client.new(host = 'parser.example.com', port = 1234 )
gnp = GNparser::Client.new(host = 'parser.example.com', port = '1234' )
# Use default 8778 port
gnp = GNparser::Client.new(host = 'parser.example.com')
# Use default 0.0.0.0 host
gnp = GNparser::Client.new(port = '1234' )
```

### Versions

To see version of the gem:

```ruby
gnp.version
```

To see version of the Go gnparser:

```ruby
ver = gnp.parser_version
puts ver.value
puts ver.build_time.size
```

### Parsing name-strings

Refer to [gnparser.proto] file to learn available fields.

All parsing methods take the following options:

``skip_cleaning:``
: if `true` names-strings get stripped from HTML tags, if there are any. For example `<i>Homo sapiens</i> <b>L.</b>` becomes `Homo sapiens L.`. It is `false` by default.

``jobs_number:`` allows to control how many jobs will run in parallel by gnparser. Note that gnparser gRPC server also has a max jobs number option. So if ``jobs_number`` is higher than gRPC MaxJobsNumber, it is ignored.

#### Parse one name

```ruby
res = gnp.parse('Puma concolor L.')
puts res.canonical.simple
puts res.authorship.value
puts res.name_type
puts res.species
```

Skipping cleaning from HTML tags:

```ruby
res = gnp.parse('<i>Puma concolor</i> (Linn.)', skip_cleaning: true)
#parsed will be false
res.parsed
...
res = gnp.parse('<i>Puma concolor</i> (Linn.)')
#parsed will be true
res.parsed
res.normalized
```

#### Parse an array of names

There is a limit of 10,000 name strings per batch. Results are always returned in the same order as input.

```ruby
names = ['Plantago major L.', 'Homo sapiens Linn. 1758', 'Bubo bubo']

# run using 3 jobs
res = gnp.parse_ary(names, jobs_number: 3)
res.each do |r|
  puts r.canonical.full
end


# do not strip HTML tags (in this case there are none in the input)
res = gnp.parse_ary(names, skip_cleaning: true)
```

[gnparser]: https://gitlab.com/gogna/gnparser
[releases]: https://gitlab.com/gogna/gnparser/releases
[gnparser.proto]: https://gitlab.com/gogna/gnparser/blob/master/pb/gnparser.proto