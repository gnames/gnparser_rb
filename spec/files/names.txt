Pseudocercospora
Pseudocercospora Speg.
Döringina Ihering 1929 (synonym)
Pseudocercospora Speg., Francis Jack.-Drake.
Aaaba de Laubenfels, 1936
Abbottia F. von Mueller, 1875
Abella von Heyden, 1826
Micropleura v Linstow 1906
Pseudocercospora Speg. 1910
Pseudocercospora Spegazzini, 1910
Tridentella tangeroae Bruce, 198?
Rhynchonellidae d'Orbigny 1847
Ataladoris Iredale & O'Donoghue 1923
Saxo-Fridericia R. H. Schomb.
Anteplana le Renard 1995
Candinia le Renard, Sabelli & Taviani 1996
Polypodium le Sourdianum Fourn.
Ca Dyar 1914
Ea Distant 1911
Ge Nicéville 1895
Ia Thomas 1902
Io Lea 1831
Io Blanchard 1852
Ix Bergroth 1916
Lo Seale 1906
Oa Girault 1929
Ra Whitley 1931
Ty Bory de St. Vincent 1827
Ua Girault 1929
Aa Baker 1940
Ja Uéno 1955
Zu Walters & Fitch 1960
La Bleszynski 1966
Qu Durkoop
As Slipinski 1982
Ba Solem 1983
Poaceae subtrib. Scolochloinae Soreng
Zygophyllaceae subfam. Tribuloideae D.M.Porter
Cordia (Adans.) Kuntze sect. Salimori
Cordia sect. Salimori (Adans.) Kuntz
Poaceae supertrib. Arundinarodae L.Liu
Alchemilla subsect. Sericeae A.Plocek
Hymenophyllum subgen. Hymenoglossum (Presl) R.M.Tryon & A.Tryon
Aconitum ser. Tangutica W.T. Wang
Calathus (Lindrothius) KURNAKOV 1961
Eucalyptus subser. Regulares Brooker
Aaleniella (Danocythere)
Notopholia corrusca
Cyathicula scelobelonium
Pseudocercospora     dendrobii
Cucurbita pepo
Hirsutëlla mâle
Aëtosaurus ferratus
Nototriton matama Boza-Oviedo, Rovito, Chaves, García-Rodríguez, Artavia, Bolaños, and Wake, 2012
Architectonica offlexa Iredale, 1931
Scilla rupestris v.d. Merwe
Bembix bidentata v.d.L.
Pompilus cinctellus v. d. L.
Setaphis viridis v. d.G.
Coleophora mendica Baldizzone & v. d.Wolf 2000
Psoronaias semigranosa von dem Busch in Philippi, 1845
Phora sororcula v d Wulp 1871
Aeolothrips andalusiacus zur Strassen 1973
Orthosia kindermannii Fischer v. Roslerstamm, 1837
Nereidavus kulkovi Kul'kov in Kul'kov & Obut, 1973
Xylaria potentillae A S. Xu
Pseudocyrtopora el Hajjaji 1987
Abacetus laevicollis de Chaudoir, 1869
Gastrosericus eremorum von Beaumont 1955
Agaricus squamula Berk. & M.A. Curtis 1860
Peltula coriacea Büdel, Henssen & Wessels 1986
Tuber liui A S. Xu 1999
Lecanora wetmorei Śliwa 2004
Vachonobisium troglophilum Vitali-di Castri, 1963
Hyalesthes angustula Horvßth, 1909
Platypus bicaudatulus Schedl (1935h)
Platypus bicaudatulus Schedl (1935)
Platypus bicaudatulus Schedl 1935
Platypus bicaudatulus Schedl, 1935h
Rotalina cultrata d'Orb. 1840
Stylosanthes guianensis (Aubl.) Sw. var. robusta L.'t Mannetje
Doxander vittatus entropi (Man in 't Veld & Visser, 1993)
Elaeagnus triflora Roxb. var. brevilimbatus E.'t Hart
Laevistrombus guidoi (Man in't Veld & De Turck, 1998)
Strombus guidoi Man in't Veld & De Turck, 1998
Strombus vittatus entropi Man in't Veld & Visser, 1993
Velutina haliotoides (Linnaeus, 1758),
M. alpium
Mo. alpium (Osbeck, 1778)
Nemcia epacridoides (Meissner)Crisp
Pseudocercospora dendrobii Goh & W.H. Hsieh 1990
Pseudocercospora dendrobii Goh and W.H. Hsieh 1990
Pseudocercospora dendrobii Goh et W.H. Hsieh 1990
Schottera nicaeënsis (J.V. Lamouroux ex Duby) Guiry & Hollenberg
Cladoniicola staurospora Diederich, van den Boom & Aptroot 2001
Stagonospora polyspora M.T. Lucas & Sousa da Câmara 1934
Stagonospora polyspora M.T. Lucas et Sousa da Câmara 1934
Pseudocercospora dendrobii U. Braun & Crous 2003
Abaxisotima acuminata (Wang, Yuwen & Xiangwei Liu 1996)
Aboilomimus sichuanensis ornatus Liu, Xiang-wei, M. Zhou, W Bi & L. Tang, 2009
Yarrowia lipolytica var. lipolytica (Wick., Kurtzman & E.A. Herrm.) Van der Walt & Arx 1981
Pseudocercospora dendrobii(H.C.     Burnett)U. Braun & Crous     2003
Pseudocercospora dendrobii(H.C.     Burnett, 1873)U. Braun & Crous     2003
Pseudocercospora dendrobii(H.C.     Burnett 1873)U. Braun & Crous ,    2003
Sedella pumila (Benth.) Britton & Rose
Impatiens nomenyae Eb.Fisch. & Raheliv.
Armeria carpetana ssp. carpetana H. del Villar
Peristernia nassatula forskali Tapparone-Canefri 1875
Cypraeovula (Luponia) amphithales perdentata
Triticum repens vulgäre
Hydnellum scrobiculatum zonatum (Batsch) K. A. Harrison 1961
Hydnellum scrobiculatum zonatum (Banker) D. Hall & D.E. Stuntz 1972
Hydnellum (Hydnellum) scrobiculatum zonatum (Banker) D. Hall & D.E. Stuntz 1972
Hydnellum scrobiculatum zonatum
Mus musculus hortulanus
Ortygospiza atricollis mülleri
Cortinarius angulatus B gracilescens Fr. 1838
Caulerpa fastigiata confervoides P. L. Crouan & H. M. Crouan ex Weber-van Bosse
Crematogaster impressa st. brazzai Santschi 1937
Cibotium st.-johnii Krajina
Camponotus conspicuus st. zonatus
Fagus sylvatica subsp. orientalis (Lipsky) Greuter & Burdet
Prunus mexicana S. Watson var. reticulata (Sarg.) Sarg.
Potamogeton iilinoensis var. ventanicola
Potamogeton iilinoensis var. ventanicola (Hicken) Horn af Rantzien
Triticum repens var. vulgäre
Aus bus Linn. var. bus
Agalinis purpurea (L.) Briton var. borealis (Berg.) Peterson 1987
Callideriphus flavicollis morph. reductus Fuchs 1961
Caulerpa cupressoides forma nuda
Chlorocyperus glaber form. fasciculariforme (Lojac.) Soó
Sphaerotheca    fuliginea    f.     dahliae    Movss.     1967
Polypodium vulgare nothosubsp. mantoniae (Rothm.) Schidlay
Allophylus amazonicus var amazonicus
Yarrowia lipolytica variety lipolytic
Prunus armeniaca convar. budae (Pénzes) Soó
Crataegus curvisepala nvar. naviculiformis T. Petauer
Polypodium pectinatum (L.) f. typica Rosenst.
Polypodium pectinatum L. f. typica Rosenst.
Polypodium pectinatum L.f. typica Rosenst.
Polypodium lineare C.Chr. f. caudatoattenuatum Takeda
Rhododendron weyrichii Maxim. f. albiflorum T.Yamaz.
Armeria maaritima (Mill.) Willd. fma. originaria Bern.
Cotoneaster (Pyracantha) rogersiana var.aurantiaca
Poa annua fo varia
Physarum globuliferum forma. flavum Leontyev & Dudka
Homalanthus nutans (Mull.Arg.) Benth. & Hook. f. ex Drake
Calicium furfuraceum * furfuraceum (L.) Pers. 1797
Polyrhachis orsyllus nat musculus Forel 1901
Acidalia remutaria ab. n. undularia
Acmaeops (Pseudodinoptera) bivittata ab. fusciceps Aurivillius, 1912
Hydnellum scrobiculatum var. zonatum f. parvum (Banker) D. Hall & D.E. Stuntz 1972
Senecio fuchsii C.C.Gmel. subsp. fuchsii var. expansus (Boiss. & Heldr.) Hayek
Senecio fuchsii C.C.Gmel. subsp. fuchsii var. fuchsii
Euastrum divergens var. rhodesiense f. coronulum A.M. Scott & Prescott
×Agropogon P. Fourn. 1934
xAgropogon P. Fourn.
XAgropogon P.Fourn.
× Agropogon
x Agropogon
X Agropogon
X Cupressocyparis leylandii
×Heucherella tiarelloides
xHeucherella tiarelloides
x Heucherella tiarelloides
×Agropogon littoralis (Sm.) C. E. Hubb. 1946
Asplenium X inexpectatum (E.L. Braun 1940) Morton (1956)
Salix ×capreola Andersson (1867)
Salix x capreola Andersson
Stanhopea tigrina Bateman ex Lindl. x S. ecornuta Lem.
Arthopyrenia hyalospora X Hydnellum scrobiculatum
Arthopyrenia hyalospora (Banker) D. Hall X Hydnellum scrobiculatum D.E. Stuntz
Arthopyrenia hyalospora x
Arthopyrenia hyalospora × ?
Agrostis L. × Polypogon Desf.
Agrostis stolonifera L. × Polypogon monspeliensis (L.) Desf.
Coeloglossum viride (L.) Hartman x Dactylorhiza majalis (Rchb. f.) P.F. Hunt & Summerhayes ssp. praetermissa (Druce) D.M. Moore & Soó
Salix aurita L. × S. caprea L.
Asplenium rhizophyllum X A. ruta-muraria E.L. Braun 1939
Asplenium rhizophyllum DC. x ruta-muraria E.L. Braun 1939
Tilletia caries (Bjerk.) Tul. × T. foetida (Wallr.) Liro.
Brassica oleracea L. subsp. capitata (L.) DC. convar. fruticosa (Metzg.) Alef. × B. oleracea L. subsp. capitata (L.) var. costata DC.
Ambystoma laterale × A. texanum × A. tigrinum
Pseudocercospora broussonetiae (Chupp & Linder) X.J. Liu & Y.L. Guo 1989
Zophosis persis (Chatanay, 1914)
Zophosis persis (Chatanay 1914)
Zophosis persis (Chatanay), 1914
Zophosis quadrilineata (Oliv. )
Zophosis quadrilineata (Olivier 1795)
Hegeter (Hegeter) tenuipunctatus Brullé, 1838
Hegeter (Hegeter) intercedens Lindberg H 1950
Cyprideis (Cyprideis) thessalonike amasyaensis
Acanthoderes (acanthoderes) satanas Aurivillius, 1923
Ferganoconcha? oblonga
Aspicilia desertorum desertorum
Theope thestias discus
Ocydromus dalmatinus dalmatinus (Dejean, 1831)
Rhipidia gracilirama lassula
Saccharomyces drosophilae anon.
Physalospora rubiginosa (Fr.) anon.
Tragacantha leporina (?) Kuntze
Lachenalia tricolor var. nelsonii (auct.) Baker
Lachenalia tricolor var. nelsonii (anon.) Baker
Puya acris anon.
Pseudocercospora dendrobii Goh apud W.H. Hsieh 1990
Arthopyrenia hyalospora (Nyl. ex Banker) R.C. Harris
Arthopyrenia hyalospora (Nyl. ex. Banker) R.C. Harris
Arthopyrenia hyalospora Nyl. ex Banker
Arthopyrenia hyalospora Nyl. ex. Banker
Glomopsis lonicerae Peck ex C.J. Gould 1945
Glomopsis lonicerae Peck ex. C.J. Gould 1945
Acanthobasidium delicatum (Wakef.) Oberw. ex Jülich 1979
Acanthobasidium delicatum (Wakef.) Oberw. ex. Jülich 1979
Mycosphaerella eryngii (Fr. ex Duby) Johanson ex Oudem. 1897
Mycosphaerella eryngii (Fr. ex. Duby) Johanson ex. Oudem. 1897
Mycosphaerella eryngii (Fr. Duby) ex Oudem. 1897
    Asplenium       X inexpectatum(E. L. Braun ex Friesner      )Morton
Drosophila obscura-x Burla, 1951
Sanogasta x-signata (Keyserling,1891)
Aedes w-albus (Theobald, 1905)
Abryna regis-petri Paiva, 1860
Solms-laubachia orbiculata Y.C. Lan & T.Y. Cheo
Oxytropis minjanensis Rech. f.
Platypus bicaudatulus Schedl f. 1935
Platypus bicaudatulus Schedl filius 1935
Fimbristylis ovata (Burm. f.) J. Kern
Carex chordorrhiza Ehrh. ex L. f.
Amelanchier arborea var. arborea (Michx. f.) Fernald
Cerastium arvense var. fuegianum Hook. f.
Cerastium arvense var. fuegianum Hook.f.
Cerastium arvense ssp. velutinum var. velutinum (Raf.) Britton f.
Jacquemontia spiciflora (Choisy) Hall. fil.
Amelanchier arborea f. hirsuta (Michx. f.) Fernald
Betula pendula fo. dalecarlica (L. f.) C.K. Schneid.
Racomitrium canescens f. ericoides (F. Weber ex Brid.) Mönk.
Racomitrium canescens forma ericoides (F. Weber ex Brid.) Mönk.
Polypodium pectinatum L. f., Rosenst.
Polypodium pectinatum L. f.
Polypodium pectinatum (L. f.) typica Rosent
Chlorobium phaeobacteroides Pfennig, 1968 emend. Imhoff, 2003
Chlorobium phaeobacteroides Pfennig, 1968 emend Imhoff, 2003
Dryopteris X separabilis Small (pro sp.)
Graphis scripta L. a.b pulverulenta
Cetraria iberica a.crespo & barreno
Lecanora achariana a.l.sm.
Arthrosporum populorum a.massal.
Eletica laeviceps ab.lateapicalis Pic
Acanthoderes 4-gibbus RILEY Charles Valentine, 1880
Acrosoma 12-spinosa Keyserling, 1892
Canuleius 24-spinosus Redtenbacher, 1906
Canuleius 777-spinosus Redtenbacher, 1906
Rhynchophorus 13punctatus Herbst, J.F.W., 1795
Rhynchophorus 13.punctatus Herbst, J.F.W., 1795
Pleurotus ëous (Berk.) Sacc. 1887
Rühlella
Sténométope laevissimus Bibron 1855
Choriozopella trägårdhi Lawrence, 1947
Isoëtes asplundii H. P. Fuchs
Cerambyx thomæ GMELIN J. F., 1790
Campethera cailliautii fülleborni
Östrupia Heiden ex Hustedt, 1935
Junellia o'donelli Moldenke, 1946
Trophon d'orbignyi Carcelles, 1946
Arca m'coyi Tenison-Woods, 1878
Nucula m'andrewii Hanley, 1860
Eristalis l'herminierii Macquart
Odynerus o'neili Cameron
Serjania meridionalis Cambess. var. o'donelli F.A. Barkley
Æschopalæa grisella Pascoe, 1864
Læptura laetifica Dow, 1913
Leptura lætifica Dow, 1913
Leptura leætifica Dow, 1913
Leæptura laetifica Dow, 1913
Leœptura laetifica Dow, 1913
Ærenea cognata Lacordaire, 1872
Œdicnemus capensis
Œnanthe œnanthe
Hördeum vulgare cœrulescens
Hordeum vulgare cœrulescens Metzger
Hordeum vulgare f. cœrulescens
Musca domeſtica Linnaeus 1758
Amphisbæna fuliginoſa Linnaeus 1758
Solygia ? distanti
Buteo borealis ? ventralis
Euxoa nr. idahoensis sp. 1clay
Acarinina aff. pentacamerata
Acarinina aff pentacamerata
Sphingomonas sp. 37
Thryothorus leucotis spp. bogotensis
Endoxyla sp. GM-, 2003
X Aegilotrichum sp.
Liopropoma sp.2 Not applicable
Lacanobia sp. nr. subjuncta Bold:Aab, 0925
Lacanobia nr. subjuncta Bold:Aab, 0925
Abturia cf. alabamensis (Morton )
Abturia cf alabamensis (Morton )
Calidris cf. cooperi
Aesculus cf. × hybrida
Daphnia (Daphnia) x krausi Flossner 1993
Barbus cf macrotaenia × toppini
Gemmula cf. cosmoi NP-2008
Coleoptera sp. BOLD:AAV0432
Coleoptera Bold:AAV0432
Arv1virus
Turtle herpesviruses
Cre expression vector
Drosophila sturtevanti rhabdovirus
Hydra expression vector
Gateway destination plasmid
Abutilon mosaic virus [X15983] [X15984] Abutilon mosaic virus ICTV
Acute bee paralysis virus [AF150629] Acute bee paralysis virus
Adeno-associated virus - 3
?M1-like Viruses Methanobrevibacter phage PG
Aeromonas phage 65
Bacillus phage SPß [AF020713] Bacillus phage SPb ICTV
Apple scar skin viroid
Australian grapevine viroid [X17101] Australian grapevine viroid ICTV
Agents of Spongiform Encephalopathies CWD prion Chronic wasting disease
Phi h-like viruses
Viroids
Fungal prions
Human rhinovirus A11
Kobuvirus korean black goat/South Korea/2010
Australian bat lyssavirus human/AUS/1998
Gossypium mustilinum symptomless alphasatellite
Okra leaf curl Mali alphasatellites-Cameroon
Bemisia betasatellite LW-2014
Tomato leaf curl Bangladesh betasatellites [India/Patna/Chilli/2008]
Intracisternal A-particles
Saccharomyces cerevisiae killer particle M1
Uranotaenia sapphirina NPV
Spodoptera exigua nuclear polyhedrosis virus SeMNPV
Spodoptera frugiperda MNPV
Rachiplusia ou MNPV (strain R1)
Orgyia pseudotsugata nuclear polyhedrosis virus OpMNPV
Mamestra configurata NPV-A
Helicoverpa armigera SNPV NNg1
Zamilon virophage
Sputnik virophage 3
Bacteriophage PH75
Escherichia coli bacteriophage
ssRNA
Alpha proteobacterium RNA12
Ustilaginoidea virens RNA virus
Candida albicans RNA_CTR0-3
Carabus satyrus satyrus KURNAKOV, 1962
Calathus (Lindrothius) KURNAKOV 1961
Fakus prioni
Crassatellites fulvida
Salmonella werahensis (Castellani) Hauduroy and Ehringer in Hauduroy 1937
Actinomyces cardiffensis
Xanthomonas axonopodis pv. phaseoli
Xanthomonas axonopodis pathovar. phaseoli
Xanthomonas axonopodis pathovar.
Xanthomonas axonopodis pv.
Pelargonium cucullatum ssp. cucullatum (L.) L'Her. ex [Soland.]
Acastella ex gr. rouaulti
Lecanora strobilinoides GIRALT & GÓMEZ-BOLEA
Astatotilapia cf. bloyeti OS-2017
Eichornia crassipes ( (Martius) ) Solms-Laub.
Nesomyrmex madecassus_01m
Hypochrys0des
Hypochrys0des Leraut 1981
Phyllodoce mucosa 0ersted, 1843
Attelabus 0l.
Acrobothrium 0lsson 1872
Staphylinus haemrrhoidalis 0l. nec Gmel
Ea92virus
Acarospora cratericola 1929
Goggia gemmula 1996
Eurodryas orientalis Herrich-Schäffer 1845-1847
Tridentella tangeroae Bruce, 1987-92
Recilia truncatus Dash & Viraktamath, 1998a: 29
Recilia truncatus Dash & Viraktamath, 1998: 29
Recilia truncatus Dash & Viraktamath, 1998a:29
Recilia truncatus Dash & Viraktamath, 1998a : 29
Anthoscopus Cabanis [1851]
Anthoscopus Cabanis [185?]
Anthoscopus Cabanis [1851?]
Anthoscopus Cabanis [1851]
Anthoscopus Cabanis [1851?]
Trismegistia monodii Ando, 1973 [1974]
Zygaena witti Wiegel [1973]
Deyeuxia coarctata Kunth, 1815 [1816]
Macrotes cordovaria Guen�e 1857
Fusinus eucos�nius
Byssochlamys fulva Olliver & G. Smith
Kinosternidae　Agassiz, 1857
Melanius:
Negalasa fumalis Barnes & McDunnough 1913. Next sentence
Negalasa fumalis. Next sentence
Negalasa fumalis, continuation of a sentence
Negalasa fumalis Barnes; something else
Negaprion brevirostris Negaprion brevirostris, the rest of the sentence
Negaprion fronto (Jordan and Gilbert, 1882):
Acanthochiton ex quisitus
Morea (Morea) Burt 2342343242 23424322342 23424234
Nautilus asterizans von
Acer 'lanum'
Labeotropheus trewavasae 'albino
Labeotropheus trewavasae albino'
Phedimus takesimensis (Nakai) 't Hart
Solanum tuberosum wila-k'oy
Solanum juzepczukii janck'o-ckaisalla
Morea (Morea) burtius 2342343242 23424322342 23424234
Verpericola megasoma ""Dall" Pils.
Verpericola megasoma "Dall" Pils.
Moraea spathulata ( (L. f. Klatt
Stewartia micrantha (Chun) Sealy, Bot. Mag. 176: t. 510. 1967.
Pyrobaculum neutrophilum V24Sta
Rana aurora Baird and Girard, 1852; H.B. Shaffer et al., 2004
Agropyron pectiniforme var. karabaljikji ined.?
Staphylococcus hyicus chromogenes Devriese et al. 1978 (Approved Lists 1980).
Adonis cyllenea Boiss. & al.
Adonis cyllenea Boiss. & al
Adonis cyllenea Boiss. & al. var. paryadrica Boiss.
Adonis cyllenea Boiss. & al var. paryadrica Boiss.
Nereidavus kulkovi 'Kulkov
Abryna -petri Paiva, 1860
Abryna petri- Paiva, 1860
Musca capraria Trustees of the British Museum (Natural History), 1939
Nassellarid genera of uncertain affinities
Natica of nidus
Neritina chemmoi Reeve var of cornea Linn
Wolbachia endosymbiont of Leptogenys gracilis
Phyllostachys vivax cv aureocaulis
Rhododendron cv Cilpinense
Ligusticum sinense cv 'chuanxiong' S.H. Qiu & et al.
Alyxia reinwardti var
Alyxia reinwardti var.
Alyxia reinwardti ssp
Alyxia reinwardti ssp.
Alaria spp
Alaria spp.
Xenodon sp
Xenodon sp.
Formicidae cf.
Formicidae cf
Arctostaphylos preglauca cf.
Acastoides spp.
Senecio legionensis sensu Samp., non Lange
Pseudomonas methanica (Söhngen 1906) sensu. Dworkin and Foster 1956
Abarema scutifera sensu auct., non (Blanco)Kosterm.
Puya acris Auct.
Puya acris Auct non L.
Galium tricorne Stokes, pro parte
Galium tricorne Stokes,pro parte
Senecio jacquinianus sec. Rchb.
Acantholimon ulicinum s.l. (Schultes) Boiss.
Acantholimon ulicinum s. l. (Schultes) Boiss.
Acantholimon ulicinum S. L. Schultes
Amitostigma formosana (S.S.Ying) S.S.Ying
Amaurorhinus bewichianus (Wollaston,1860) (s.str.)
Ammodramus caudacutus (s.s.) diversus
Arenaria serpyllifolia L. s.str.
Asplenium trichomanes L. s.lat. - Asplen trich
Asplenium anisophyllum Kunze, s.l.
Abramis Cuvier 1816 sec. Dybowski 1862
Abramis brama subsp. bergi Grib & Vernidub 1935 sec Eschmeyer 2004
Abarema clypearia (Jack) Kosterm., P. P.
Abarema clypearia (Jack) Kosterm., p.p.
Abarema clypearia (Jack) Kosterm., p. p.
Indigofera phyllogramme var. aphylla R.Vig., p.p.B
Asplenium mayi ht.May; Gard.
Asplenium mayii ht.May; Gard.
Davallia decora ht.Bull.; Gard.Chr.
Gymnogramma alstoni ht.Birkenh.; Gard.
Gymnogramma sprengeriana ht.Wiener Ill.
Amphiprora pseudoduplex (Osada & Kobayasi, 1990) comb. nov.
Methanosarcina barkeri str. fusaro
Arthopyrenia hyalospora (Nyl.) R.C. Harris comb. nov.
Acanthophis lancasteri WELLS & WELLINGTON (nomen nudum)
Acontias lineatus WAGLER 1830: 196 (nomen nudum)
Akeratidae Nomen Nudum
Aster exilis Ell., nomen dubium
Abutilon avicennae Gaertn., nom. illeg.
Achillea bonarota nom. in herb.
Aconitum napellus var. formosum (Rchb.) W. D. J. Koch (nom. ambig.)
Aesculus canadensis Hort. ex Lavallée
× Dialaeliopsis hort.
Velutina haliotoides (Linnaeus, 1758), sensu Fabricius, 1780
Acarospora cratericola cratericola Shenk 1974 group
Acarospora cratericola cratericola Shenk 1974 species group
Acarospora cratericola cratericola Shenk 1974 species complex
Parus caeruleus species complex
Lachenalia tricolor var. nelsonii (ht.) Baker
Lachenalia tricolor var. nelsonii (hort.) Baker
Puya acris ht.
Puya acris hort.
Mom.alpium (Osbeck, 1778)
Gen. et n. sp. Kaimatira Pumice Sand, Marton N ~1 Ma
Genn. et n. sp. Kaimatira Pumice Sand, Marton N ~1 Ma
Incertae sedis
</i>Hipponicidae<i> incertae sedis</i>
incertae sedis
Inc.   sed.
inc.sed.
inc.   sed.
None recorded
NONE recorded
NoNe recorded
unidentified recorded
UniDentiFied recorded
not recorded
NOT recorded
Not recorded
Not assigned
Notassigned
Abbott's moray eel
Chambers' twinpod
Columnea × Alladin's
Hawai'i silversword
PomaTomus
DizygopUwa stosei
Oxytox[idae] Lindermann
ScarabaeinGCsp.
Alfalfa witches'-broom phytoplasma
Allium ampeloprasumphytoplasma
Alstroemeria sp. phytoplasma
Lampona spec Platnick, 2000
