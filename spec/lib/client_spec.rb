# frozen_string_literal: true

describe GNparser::Client do
  let(:subject) { GNparser::Client.new }

  describe '.new' do
    it 'breaks connection if go gnparser is too old' do
      stub_const('GNparser::Client::PARSER_MIN_VERSION', 'v100.100.100')
      expect { GNparser::Client.new }.to raise_error(/gRPC server/)
    end

    it 'connects if go gnparser is too young' do
      stub_const('GNparser::Client::PARSER_MIN_VERSION', 'v0.0.0')
      expect { GNparser::Client.new }.to_not raise_error
    end
  end

  describe '#parser_version' do
    it 'connects to the server' do
      ver = subject.parser_version
      expect(ver.value).to match(/^v\d/)
      expect(ver.build_time.size).to be > 5
    end
  end

  describe '#parse' do
    it 'parses one name' do
      res = subject.parse('Pomatomus saltator')
      expect(res.normalized).to eq 'Pomatomus saltator'
      expect(res.cardinality).to eq 2
    end

    it 'parses one name cleaning html tags' do
      res = subject.parse('<i>Pomatomus saltator</i>')
      expect(res.normalized).to eq 'Pomatomus saltator'
    end

    it 'optionally does not clean html tags' do
      res = subject.parse('<i>Pomatomus saltator</i>', skip_cleaning: true)
      expect(res.normalized).to eq ''
      expect(res.parsed).to be false
    end

    it 'ignores unknown options' do
      res = subject.parse('Pomatomus saltator', something: 'one', a: 2)
      expect(res.parsed).to be == true
    end
  end

  describe '#parse_ary' do
    it 'does not parse if there are too many names' do
      names = (1..51_000).each_with_object([]) do |_, a|
        a << 'Some name'
      end

      expect { subject.parse_ary(names) }
        .to raise_error(/keep input smaller/)
    end

    it 'does not parse if input is empty' do
      expect { subject.parse_ary([]) }.to raise_error(/empty input/)
    end

    it 'parses names in exact order' do
      names = ['Pardosa moesta Banks, 1892', 'Pomatomus saltator',
               'Homo sapiens L.', 'Bubo bubo',
               'Monochamus galloprovincialis',
               'Potentila erecta form erecta 1888',
               'Tobacco Mosaic Virus']
      res = subject.parse_ary(names)
      res.each_with_index do |e, i|
        expect(e.verbatim).to eq names[i]
      end
    end

    it 'parses an array of names' do
      ary = ['Homo', 'Puma concolor', 'Bubo bubo L.']
      res = subject.parse_ary(ary)
      count = 0
      res.each do |r|
        count += 1
        expect(r.parsed).to be true
      end
      expect(count).to be == 3
    end

    it 'cleans names from html tags' do
      names = ['<i>Bubo bubo</i> <b>L.</b>']
      res = subject.parse_ary(names)
      expect(res[0].verbatim).to eq '<i>Bubo bubo</i> <b>L.</b>'
      expect(res[0].normalized).to eq 'Bubo bubo L.'
    end
  end
end
