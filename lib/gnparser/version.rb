# frozen_string_literal: true

# GNparser is a namespace module for gnparser gem.
module GNparser
  VERSION = '0.14.1'

  def self.version
    VERSION
  end
end
