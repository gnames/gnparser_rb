# frozen_string_literal: true

module GNparser
  # Gnfinder::Client connects to gnfinder server
  class Client
    PARSER_MIN_VERSION = 'v0.14.1'

    def initialize(host = 'parser-rpc.globalnames.org', port = '80')
      @stub = Pb::GNparser::Stub.new("#{host}:#{port}",
                                     :this_channel_is_insecure)
      return if supported_parser(parser_version.value, PARSER_MIN_VERSION)

      raise 'gRPC server of gnparser should be at least ' \
            " #{PARSER_MIN_VERSION}.\n Download latest version from " \
            'https://gitlab/gogna/gnparser/releases. '
    end

    # checks if gRPC's gnparser version is compatible
    def supported_parser(version, min_version)
      min_ver = min_version[1..].split('.').map(&:to_i)
      ver = version[1..].split('.').map(&:to_i)
      return true if ver[0] > min_ver[0] || ver[1] > min_ver[1]

      return true if ver[2] >= min_ver[2]

      false
    end

    # parser_version retrieves the version of gnparser used by gRPC service.
    def parser_version
      @stub.ver(Pb::Void.new)
    end

    # parse parses one name at a time and returns protocol buffer object with
    # the results. If the name-string contains HTML tags they will be stripped
    # by gRPC method.
    def parse(name, opts = {})
      parse_ary([name], opts)[0]
    end

    # parse_ary parses an array of name-strings and returns an array of
    # protocol buffer objects with the results. The array preserves the same
    # order of elements. If the name-string contains HTML tags they will be
    # stripped internally by the gnparser.
    def parse_ary(names, opts = {})
      input = Pb::InputArray.new
      input.names += names
      input.skip_cleaning = true if opts[:skip_cleaning]
      jobs = opts[:jobs_number].to_i
      input.jobs_number = jobs if jobs.positive?
      res = @stub.parse_array(input)
      res.output
    end
  end
end
