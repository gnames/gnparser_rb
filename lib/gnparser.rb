# frozen_string_literal: true

require 'json'
require_relative 'gnparser/version'
require_relative 'gnparser_pb.rb'
require_relative 'gnparser_services_pb.rb'
require_relative 'gnparser/client'

# GNparser is a namespace module for gnparser gem.
module GNparser
end
