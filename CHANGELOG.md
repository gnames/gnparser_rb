# Changelog

## Unreleased


## [v0.1.0]

- Add [#1]: connect to gRPC server of `gnparser` (Go version).

## Footnotes

This document follows [changelog guidelines]

[v0.1.0]: https://gitlab.com/gnames/gnparser_rb/tree/v0.1.0

[#1]: https://gitlab.com/gnames/gnparser_rb/issues/1


[changelog guidelines]: https://github.com/olivierlacan/keep-a-changelog
